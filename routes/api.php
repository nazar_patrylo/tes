<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\LessonController;
use App\Http\Controllers\API\PupilController;
use App\Http\Controllers\API\TeacherController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['api'],
], function ($router) {
    Route::post('/lesson', [LessonController::class, 'create']);
    Route::post('/lesson/pupil', [LessonController::class, 'attachPupil']);
    Route::post('/lesson/teacher', [LessonController::class, 'attachTeacher']);
    Route::post('/pupil', [PupilController::class, 'create']);
    Route::post('/teacher', [TeacherController::class, 'create']);
    Route::get('/teacher', [TeacherController::class, 'search']);
});
