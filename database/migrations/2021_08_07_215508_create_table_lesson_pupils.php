<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLessonPupils extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_pupils', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lesson_id')
                ->references('id')
                ->on('lessons')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('pupil_id')
                ->references('id')
                ->on('pupils')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unique(['lesson_id', 'pupil_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_pupils');
    }
}
