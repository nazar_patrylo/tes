<?php

namespace App\Repositories;

use App\Models\Teacher;

class TeacherRepository
{
    public function create(array $data):Teacher
    {
        return Teacher::create($data);
    }
}
