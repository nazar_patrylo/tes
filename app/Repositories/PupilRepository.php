<?php

namespace App\Repositories;

use App\Models\Pupil;

class PupilRepository
{

    public function create(array $data): Pupil
    {
        return Pupil::create($data);
    }

    public function getById(int $id): ?Pupil
    {
        return Pupil::query()
            ->with('lessons', 'lessons.teachers')
            ->where('id', '=', $id)
            ->first();
    }
}
