<?php

namespace App\Repositories;

use App\Models\Lesson;
use App\Models\Pupil;

class LessonRepository
{

    public function create(array $data):Lesson
    {
        return Lesson::create($data);
    }

    public function getById(int $id): ?Lesson
    {
        return Lesson::where('id', '=', $id)->first();
    }
}
