<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function pupiles()
    {
        return $this->belongsToMany(Pupil::class, 'lesson_pupils');
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class, 'lesson_teachers');
    }
}
