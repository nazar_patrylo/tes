<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pupil extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'year'
    ];

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class, 'lesson_pupils');
    }
}
