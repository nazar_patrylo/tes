<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeacherRequest;
use App\Http\Requests\TeacherSearchRequest;
use App\Http\Responses\TeacherResponse;
use App\Services\TeacherService;

class TeacherController extends Controller
{
    protected TeacherService $teacherService;

    /**
     * @param TeacherService $teacherService
     */
    public function __construct(TeacherService $teacherService)
    {
        $this->teacherService = $teacherService;
    }

    public function create(TeacherRequest $request): TeacherResponse
    {
        $pupil = $this->teacherService->create($request->validated());
        return TeacherResponse::item($pupil)->addMessage($pupil ? 'Teacher was created' : 'Teacher was not created');
    }

    public function search(TeacherSearchRequest $request)
    {
        $teachers = $this->teacherService->findTeachersByPupil($request->get('pupil_id'));

        return TeacherResponse::collection($teachers);
    }
}
