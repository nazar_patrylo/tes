<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\LessonAttachPupilRequest;
use App\Http\Requests\LessonAttachTeacherRequest;
use App\Http\Requests\LessonRequest;
use App\Http\Responses\LessonResponse;
use App\Http\Responses\Response;
use App\Services\LessonService;

class LessonController extends Controller
{
    protected LessonService $lessonService;

    /**
     * @param LessonService $lessonService
     */
    public function __construct(LessonService $lessonService)
    {
        $this->lessonService = $lessonService;
    }

    public function create(LessonRequest $request): LessonResponse
    {
        $lesson = $this->lessonService->create($request->validated());
        return LessonResponse::item($lesson)->addMessage($lesson ? 'Lesson was created' : 'lesson was not created');
    }

    public function attachPupil(LessonAttachPupilRequest $request)
    {
        $status = $this->lessonService->attachPupil($request->get('lesson_id'), $request->get('pupil_id'));

        return Response::item()->addMessage($status ? 'Pupil was attached to lesson' : 'Pupil was not attached to lesson');
    }

    public function attachTeacher(LessonAttachTeacherRequest $request)
    {
        $status = $this->lessonService->attachTeacher($request->get('lesson_id'), $request->get('teacher_id'));

        return Response::item()->addMessage($status ? 'Teacher was attached to lesson' : 'Teacher was not attached to lesson');
    }
}
