<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PupilRequest;
use App\Http\Responses\PupilResponse;
use App\Services\PupilService;

class PupilController extends Controller
{
    protected PupilService $pupilService;

    /**
     * @param PupilService $pupilService
     */
    public function __construct(PupilService $pupilService)
    {
        $this->pupilService = $pupilService;
    }

    public function create(PupilRequest $request): PupilResponse
    {
        $pupil = $this->pupilService->create($request->validated());
        return PupilResponse::item($pupil)->addMessage($pupil ? 'Pupil was created' : 'Pupil was not created');
    }
}
