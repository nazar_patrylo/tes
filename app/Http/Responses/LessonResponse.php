<?php

namespace App\Http\Responses;

use App\Models\Lesson;

class LessonResponse extends Response
{
    protected function transform(Lesson $lesson)
    {
        return [
            'id' => $lesson->id,
            'name' => $lesson->name
        ];
    }
}
