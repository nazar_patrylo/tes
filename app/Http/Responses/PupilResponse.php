<?php

namespace App\Http\Responses;

use App\Models\Pupil;

class PupilResponse extends Response
{
    protected function transform(Pupil $pupil)
    {
        return [
            'id' => $pupil->id,
            'name' => $pupil->name,
            'year' => $pupil->year
        ];
    }
}
