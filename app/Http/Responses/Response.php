<?php


namespace App\Http\Responses;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\ResponseTrait;
use Illuminate\Support\Collection;

class Response implements Responsable, Arrayable
{
    use ResponseTrait;

    protected $code = 200;
    protected $data = [];
    protected $additional = [];
    protected $message = '';
    protected $response;
    protected $errors = [];

    public function __construct()
    {
        $this->response = response();
    }

    public function additional(array $additional)
    {
        $this->additional = $additional;
        return $this;
    }

    public function code($status)
    {
        $this->code = $status;
        return $this;
    }

    protected function process()
    {
        if ($this->data instanceof Collection || is_array($this->data)) {
            foreach ($this->data as $id => $element) {
                if (is_array($element)) {
                    return false;
                }
                $this->data[$id] = $this->processElement($element);
            }
        } else {
            $this->data = $this->processElement($this->data);
        }
    }

    protected function processElement($element)
    {
        $data = [];

        if (method_exists($this, 'transform') && !is_null($element)) {
            $data = array_merge($data, $this->transform($element));
        } else {
            $data = $element;
        }

        if ($element instanceof Model) {
            foreach ($element->getRelations() as $relationsName => $relation) {
                $methodName = 'transform' . ucfirst($relationsName);
                if (method_exists($this, $methodName)) {
                    $data = array_merge($data, [
                        $relationsName => $this->$methodName($relation),
                    ]);
                }
            }
        }
        return $data;
    }

    protected function dateTimeFormat(Carbon $carbon = null)
    {
        return $carbon ? $carbon->toDateTimeString() : null;
    }

    protected function dateFormat(Carbon $carbon = null)
    {
        return $carbon ? $carbon->toDateString() : null;
    }

    public function getData()
    {
        return $this->data;
    }

    public static function create()
    {
        return new static();
    }

    /**
     * @param array|LengthAwarePaginator|null|Collection $elements
     * @return Response
     */
    public static function collection($elements = null)
    {
        $response = static::create();

        if ($elements instanceof LengthAwarePaginator) {
            $response->data = $elements->items();

            $response->additional([
                'paginate' => [
                    'current_page' => $elements->currentPage(),
                    'total_page' => $elements->lastPage(),
                    'per_page' => $elements->perPage(),
                    'total_elements' => $elements->total(),
                ],
            ]);
        } else {
            $response->data = $elements;
        }
        $response->process();
        return $response;
    }

    public static function item($element = null)
    {
        $response = static::create();
        $response->data = $element;
        $response->process();
        return $response;
    }

    public function toResponse($request = null)
    {
        $data['data'] = $this->data;

        if($this->message){
            $data['message'] = $this->message;
        }

        if($this->errors){
            $data['errors'] = $this->groupErrors();
        }

        $allData = isset($data) ? array_merge($data, $this->additional) : $this->additional;

        return $this->response->json($allData, $this->code);
    }

    protected function groupErrors()
    {
        $grouped = [];

        foreach ($this->errors as $key => $val){
            $grouped[$key] = is_array($val) ? implode(", ",$val) : $val;
        }

        return $grouped;
    }

    public function addMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function addErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    public function toArray(): array
    {
        return json_decode(json_encode($this->getData()), true);
    }
}
