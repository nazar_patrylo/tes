<?php

namespace App\Http\Responses;

use App\Models\Teacher;

class TeacherResponse extends Response
{
    protected function transform(Teacher $teacher)
    {
        return [
            'id' => $teacher->id,
            'name' => $teacher->name
        ];
    }
}
