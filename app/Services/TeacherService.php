<?php

namespace App\Services;

use App\Models\Lesson;
use App\Models\Teacher;
use App\Repositories\PupilRepository;
use App\Repositories\TeacherRepository;
use Illuminate\Support\Collection;

class TeacherService
{
    protected TeacherRepository $teacherRepository;
    protected PupilRepository $pupilRepository;

    /**
     * @param TeacherRepository $teacherRepository
     * @param PupilRepository $pupilRepository
     */
    public function __construct(TeacherRepository $teacherRepository, PupilRepository $pupilRepository)
    {
        $this->teacherRepository = $teacherRepository;
        $this->pupilRepository = $pupilRepository;
    }


    public function create(array $data): Teacher
    {
        return $this->teacherRepository->create($data);
    }

    public function findTeachersByPupil(int $pupilId): Collection
    {
        $pupil = $this->pupilRepository->getById($pupilId);

        $teachers = collect();

        $pupil->lessons->each(function (Lesson $lesson) use (&$teachers) {
            if ($lesson->teachers) {
                $lesson->teachers->each(function (Teacher $teacher) use (&$teachers) {
                    $teachers->push($teacher);
                 });
            }
        });

        return $teachers;
    }
}
