<?php

namespace App\Services;

use App\Models\Pupil;
use App\Repositories\PupilRepository;

class PupilService
{
    protected PupilRepository $pupilRepository;

    /**
     * @param PupilRepository $pupilRepository
     */
    public function __construct(PupilRepository $pupilRepository)
    {
        $this->pupilRepository = $pupilRepository;
    }

    public function create(array $data): Pupil
    {
        return $this->pupilRepository->create($data);
    }
}
