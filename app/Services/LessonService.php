<?php

namespace App\Services;

use App\Models\Lesson;
use App\Repositories\LessonRepository;
use App\Repositories\PupilRepository;
use App\Repositories\TeacherRepository;

class LessonService
{
    protected LessonRepository $lessonRepository;
    protected PupilRepository $pupilRepository;
    protected TeacherRepository $teacherRepository;

    /**
     * @param LessonRepository $lessonRepository
     * @param PupilRepository $pupilRepository
     * @param TeacherRepository $teacherRepository
     */
    public function __construct(LessonRepository $lessonRepository, PupilRepository $pupilRepository, TeacherRepository $teacherRepository)
    {
        $this->lessonRepository = $lessonRepository;
        $this->pupilRepository = $pupilRepository;
        $this->teacherRepository = $teacherRepository;
    }


    public function create(array $data): Lesson
    {
        return $this->lessonRepository->create($data);
    }

    public function attachPupil(int $lessonId, int $pupilId): bool
    {
        try {
            $lesson = $this->lessonRepository->getById($lessonId);
            $lesson->pupiles()->attach($pupilId);

            return true;
        } catch (\Throwable $e){
            return false;
        }
    }

    public function attachTeacher(int $lessonId, int $teacherId): bool
    {
        try {
            $lesson = $this->lessonRepository->getById($lessonId);
            $lesson->teachers()->attach($teacherId);

            return true;
        } catch (\Throwable $e){
            return false;
        }
    }
}
